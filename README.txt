Drill UI Script
- These are the UI scripts and UI manager scripts that we used in out Game for the first 12 months during pre-production and alpha. After alpha, we redesigned the UI, and along with it some of theses scripts.

Project Hades
- A personal Isometric top down turn based RPG. This file includes the test level, along with Nav Mesh scripts and mapping in Unity, point and click controls that use the Nav Mesh for navigation, and object selection and highlight.
- Some assets are self created, other are free assets from the Unity store to help with prototyping
- Work in progress

TurnBasedPrototype
- The prototype and scripts for the turn based system. Base system and STATE machine are created, along with Item Database editor that reads from either JSON files or XML files.
- Work in progress

Object Randomizer
- a Unity tool that I created to help create and randomize object placement in the level. Will allow the designer to manually choose prefeb and/or material or to generate off of a list in any random order, as well as position, amount, rotation and distance from the center point.
